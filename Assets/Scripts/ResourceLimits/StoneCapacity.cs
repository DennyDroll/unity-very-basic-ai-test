﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneCapacity : MonoBehaviour {

    public static int maxRes;

    private void Start()
    {
        maxRes = 100;
    }
    private void FixedUpdate()
    {
        if (maxRes == 0) { Destroy(this.gameObject); }
    }

}
