﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatherResource : MonoBehaviour {

    public GameObject resource;
    public GameObject cityCenter;
    public GameObject Hut;
    public GameObject cityDeploy;
    public int capacity;
    public int inStock;
    public int miningSpeed;
    public int walkingSpeed;
    public float step;
    public Vector3 hutPos;
    public Vector3 gatherPos;
    public Vector3 deployPos;



    // Use this for initialization
    void Start()
    {
        if (VillagerController.profession == "stonemason") { resource = GameObject.FindGameObjectWithTag("stone"); MoveToHut(); }
        if (VillagerController.profession == "woodchopper") { resource = GameObject.FindGameObjectWithTag("tree"); MoveToHut(); }
        cityCenter = GameObject.FindGameObjectWithTag("cityCenter");

        // define different Positions
        hutPos.x = Hut.transform.position.x;
        hutPos.y = 0.5f;
        hutPos.z = Hut.transform.position.z;

        gatherPos.x = resource.transform.position.x;
        gatherPos.y = 0.5f;
        gatherPos.z = resource.transform.position.z;

        deployPos.x = cityDeploy.transform.position.x;
        deployPos.y = 0.5f;
        deployPos.z = cityDeploy.transform.position.z;

        step = walkingSpeed * Time.deltaTime;
        inStock = 0;
        miningSpeed = 1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (CityController.stone < CityController.maxStone)
        {
            if (inStock == 0) { MoveToGather(); }
            if (inStock == capacity)
            {
                MoveToDeploy();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "stone" && inStock == 0) { Mining(); }
        if (other.gameObject.tag == "tree" && inStock == 0) { Chopping(); }
        if (other.gameObject.tag == "cityCenter" && inStock >= capacity) { Deploy(); }
    }

    public void MoveToGather()
    {
        transform.position = Vector3.MoveTowards(transform.position, gatherPos, step);
    }
    public void MoveToDeploy()
    {
        transform.position = Vector3.MoveTowards(transform.position, deployPos, step);
    }
    public void MoveToHut()
    {
        transform.position = Vector3.MoveTowards(transform.position, gatherPos, step);
    }

    public void Mining()
    {
        for (int i = 0; i < capacity; i++)
        {
            while (inStock < capacity)
            {
                inStock = inStock + miningSpeed;
                StoneCapacity.maxRes--;
            }
        }
    }
    public void Chopping()
    {
        for (int i = 0; i < capacity; i++)
        {
            while (inStock < capacity)
            {
                inStock = inStock + miningSpeed;
                TreeCapacity.maxRes--;
            }
        }
    }

    public void Deploy()
    {
        if (this.gameObject.tag == "stonemason") { CityController.stone = CityController.stone + inStock; }
        if (this.gameObject.tag == "woodchopper") { CityController.wood = CityController.wood + inStock; }

        inStock = 0;
    }
}
