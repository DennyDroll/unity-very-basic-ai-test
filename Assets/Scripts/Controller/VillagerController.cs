﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerController : MonoBehaviour {

    public GameObject resource;
    public GameObject cityCenter;
    public GameObject pickaxe;
    public GameObject axe;
    public static string profession;
    public int profInt;
    public int vilNr;
    public float speed;
    public float step;
    public Vector3 newPos;

    private void Start()
    {
        this.vilNr = CityController.vilNr++;   
        cityCenter = GameObject.FindGameObjectWithTag("cityCenter");
        pickaxe = GameObject.FindGameObjectWithTag("pickaxe");
        axe = GameObject.FindGameObjectWithTag("axe");
        step = speed * Time.deltaTime;
        newPos.x = transform.position.x;
        newPos.y = 0.5f;
        newPos.z = transform.position.z;
        profInt = Random.Range(0, 2);
        if (profInt == 0) { profession = "stonemason"; resource = GameObject.FindGameObjectWithTag("stone"); }
        if (profInt == 1) { profession = "woodchopper"; resource = GameObject.FindGameObjectWithTag("tree"); }

        Debug.Log("Hi, I'm Villager Nr. " + this.vilNr + " and my profession is " + profession);
    }

    private void FixedUpdate()
    {

        if (profInt == 0)
        {
            pickaxe = GameObject.FindGameObjectWithTag("pickaxe");
            newPos.x = pickaxe.transform.position.x;
            newPos.y = 0.5f;
            newPos.z = pickaxe.transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, newPos, step);
        }

        if (profInt == 1)
        {
            axe = GameObject.FindGameObjectWithTag("axe");
            newPos.x = axe.transform.position.x;
            newPos.y = 0.5f;
            newPos.z = axe.transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, newPos, step);
        }
    }
}
