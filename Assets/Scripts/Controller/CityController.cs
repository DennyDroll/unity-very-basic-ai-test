﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityController : MonoBehaviour {
    // static Variables
    public static int vilNr;
    public static int stone = 20;
    public static int wood = 20;
    public static int food = 20;
    public static int maxStone = stone * 10;

    // Non stativ Variables
    public GameObject villager;    
    public GameObject stonemason;
    public float gridX = 2f;
    public float gridZ = 5f;
    public float spacing = 0.6f;
    public GameObject cityCenter;
    public Text stoneText;
    public Text woodText;
    public Text foodText;

    void Start()
    {
        SetUiText(); 
        // Instantiate Villager
        for (int z = 0; z < gridZ; z++)
        {
            for (int x = 0; x < gridX; x++)
            {
                Vector3 pos = new Vector3(x, 0.5f, z) * spacing;
                pos.x = pos.x + cityCenter.transform.position.x - 1.5f;
                pos.z = pos.z - 1.2f;
                Instantiate(villager, pos, Quaternion.identity);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        SetUiText();
    }

    void SetUiText()
    {
        stoneText.text = "Stone: " + stone.ToString();
        woodText.text = "Wood: " + wood.ToString();
        foodText.text = "Food: " + food.ToString();
    }
}
