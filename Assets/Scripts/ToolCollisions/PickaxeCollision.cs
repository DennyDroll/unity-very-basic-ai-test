﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickaxeCollision : MonoBehaviour
{
    public GameObject stonemason;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "villager" && VillagerController.profession == "stonemason")
        {
            Destroy(this.gameObject);
            Vector3 posOfnew = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Instantiate(stonemason, posOfnew, Quaternion.identity);
        }
    }
}
